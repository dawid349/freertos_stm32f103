/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct{
	int age;
	float weight;
	char* name;

}Person;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint8_t timer_updates;
extern volatile uint8_t task_counter;
extern volatile int soft_timer_callbacks;
volatile uint32_t task2_count = 0;
uint8_t semafore_count = 0;
uint16_t data_from_queque[5]={0};
uint16_t data_to_send[5] = {37,38,29,23,51};

Person Dawid = {13,45.43,"Dawid"};
/* USER CODE END Variables */
/* Definitions for ReadInputs */
osThreadId_t ReadInputsHandle;
const osThreadAttr_t ReadInputs_attributes = {
  .name = "ReadInputs",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for myTask02 */
osThreadId_t myTask02Handle;
const osThreadAttr_t myTask02_attributes = {
  .name = "myTask02",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};
/* Definitions for myQueue01 */
osMessageQueueId_t myQueue01Handle;
const osMessageQueueAttr_t myQueue01_attributes = {
  .name = "myQueue01"
};
/* Definitions for myTimer01 */
osTimerId_t myTimer01Handle;
const osTimerAttr_t myTimer01_attributes = {
  .name = "myTimer01"
};
/* Definitions for myMutex01 */
osMutexId_t myMutex01Handle;
const osMutexAttr_t myMutex01_attributes = {
  .name = "myMutex01"
};
/* Definitions for myBinarySem01 */
osSemaphoreId_t myBinarySem01Handle;
const osSemaphoreAttr_t myBinarySem01_attributes = {
  .name = "myBinarySem01"
};
/* Definitions for myCountingSem01 */
osSemaphoreId_t myCountingSem01Handle;
const osSemaphoreAttr_t myCountingSem01_attributes = {
  .name = "myCountingSem01"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void StartTask02(void *argument);
void Callback01(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of myMutex01 */
  myMutex01Handle = osMutexNew(&myMutex01_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of myBinarySem01 */
  myBinarySem01Handle = osSemaphoreNew(1, 1, &myBinarySem01_attributes);

  /* creation of myCountingSem01 */
  myCountingSem01Handle = osSemaphoreNew(5, 5, &myCountingSem01_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* creation of myTimer01 */
  myTimer01Handle = osTimerNew(Callback01, osTimerPeriodic, NULL, &myTimer01_attributes);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  osTimerStart(myTimer01Handle, 100);
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of myQueue01 */
  myQueue01Handle = osMessageQueueNew (16, sizeof(data_from_queque), &myQueue01_attributes);


  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of ReadInputs */
  ReadInputsHandle = osThreadNew(StartDefaultTask, NULL, &ReadInputs_attributes);

  /* creation of myTask02 */
  myTask02Handle = osThreadNew(StartTask02, NULL, &myTask02_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  osThreadNew(StartTask02,(void*)(&Dawid), &myTask02_attributes);
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the ReadInputs thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
	/*if(!(osSemaphoreAcquire(myBinarySem01Handle, 1000)))
	{	//  osSemaphoreWait(semaphore_id, millisec)
		task_counter++;

	}//osDelay(2);
	*/
	 /* if(osSemaphoreGetCount(myCountingSem01Handle) == 5)
	  {
		  task_counter++;
		  osSemaphoreAcquire(myCountingSem01Handle, 1000);
		  osSemaphoreAcquire(myCountingSem01Handle, 1000);
		  osSemaphoreAcquire(myCountingSem01Handle, 1000);
		  osSemaphoreAcquire(myCountingSem01Handle, 1000);
		  osSemaphoreAcquire(myCountingSem01Handle, 1000);

	  }
	  osDelay(2);
		 */

		if (!(osMutexAcquire(myMutex01Handle, osWaitForever))) {	//  osSemaphoreWait(semaphore_id, millisec)
			task_counter++;
			osMutexRelease(myMutex01Handle);

		}

		osDelay(2);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void *argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */
  for(;;)
  {
	osDelay(1000);
	task2_count = osMessageQueueGetMsgSize(myQueue01Handle);//((Person*)argument)->age;

	//osMessagePut(myQueue01Handle, data_to_send, 100);
	osMessageQueuePut(myQueue01Handle,data_to_send, 0, 100);
	data_to_send[0]++;

	//osDelay(1000);
	//task2_count = (int)argument;
	//osMutexRelease(myMutex01Handle);
    //osDelay(1000);
  }
  /* USER CODE END StartTask02 */
}

/* Callback01 function */
void Callback01(void *argument)
{
  /* USER CODE BEGIN Callback01 */
	soft_timer_callbacks++;
	//osSemaphoreRelease(myBinarySem01Handle);
	//osMutexRelease(myMutex01Handle);
	//osSemaphoreRelease(myCountingSem01Handle);
	//semafore_count = osSemaphoreGetCount(myCountingSem01Handle);
	//osSemaphoreGetCount(semaphore_id)
	//osEvent evt;
	//evt = osMessageGet(myQueue01Handle, 1);

	osMessageQueueGet(myQueue01Handle, data_from_queque, 0, 1);

	/* USER CODE END Callback01 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
